use idpwmemo_rs::{Error, IDPWMemo};
use std::env;
use std::fs;
use std::io;

const TIME_ZONE: i64 = 9; // Asia/Tokyo +9:00

fn main() -> io::Result<()> {
    let args: Vec<_> = env::args().skip(1).take(3).collect();

    match args.as_slice() {
        [file] => listup_services(file),
        [file, service_name] => view_service(file, service_name, 1),
        [file, service_name, nth] => {
            let nth: i32 = match nth.parse() {
                Ok(nth) => nth,
                _ => {
                    eprintln!("[ERROR]: <n-th> is not number");
                    return show_usage();
                }
            };
            view_service(file, service_name, nth)
        }
        _ => show_usage(),
    }
}

fn show_usage() -> io::Result<()> {
    let exe = env::current_exe()?
        .file_name()
        .and_then(|f| f.to_str())
        .unwrap_or("idpwmemo_view_cli_rs")
        .to_string();

    println!("{}", exe);
    println!("  IDPWMemo data file viewer");
    println!("usage");
    println!("  {} <memo_file>", exe);
    println!("    list up service names in the memo");
    println!("  {} <memo_file> <service_name>", exe);
    println!("    show the values of the first service found to partially match <service_name>");
    println!("  {} <memo_file> <service_name> <n-th>", exe);
    println!("    show the values of the <n-th> service found to partially match <service_name>");

    Ok(())
}

fn get_password() -> io::Result<String> {
    print!("password: ");
    io::Write::flush(&mut io::stdout())?;

    let mut password = String::new();
    io::stdin().read_line(&mut password)?;

    println!();

    Ok(password.trim_end_matches(|c| c == '\r' || c == '\n').into())
}

fn to_io_error(error: Error) -> io::Error {
    match error {
        Error::IoError(io_error) => io_error,
        error => io::Error::new(io::ErrorKind::Other, error),
    }
}

fn listup_services(file: &str) -> io::Result<()> {
    let buf = fs::read(file)?;
    let password = get_password()?;
    let mut idpw_memo: IDPWMemo = Default::default();

    idpw_memo
        .set_password(password.as_bytes())
        .map_err(to_io_error)?;

    idpw_memo.load_memo(&buf).map_err(to_io_error)?;

    let names = idpw_memo.get_service_names().map_err(to_io_error)?;

    for (i, n) in names.iter().enumerate() {
        println!("{:3}: {}", i, n);
    }

    Ok(())
}

fn view_service(file: &str, service_name: &str, mut nth: i32) -> io::Result<()> {
    let buf = fs::read(file)?;
    let password = get_password()?;
    let mut idpw_memo: IDPWMemo = Default::default();

    idpw_memo
        .set_password(password.as_bytes())
        .map_err(to_io_error)?;

    idpw_memo.load_memo(&buf).map_err(to_io_error)?;

    let names = idpw_memo.get_service_names().map_err(to_io_error)?;

    let mut index: Option<usize> = None;

    for (i, n) in names.iter().enumerate() {
        if n.contains(service_name) {
            index = Some(i);
            nth -= 1;
            if nth == 0 {
                break;
            }
        }
    }

    let index = index.ok_or_else(|| {
        io::Error::new(
            io::ErrorKind::Other,
            format!("not found servic_ename ({})", service_name),
        )
    })?;

    idpw_memo.select_service(index).map_err(to_io_error)?;

    let values = idpw_memo.values().map_err(to_io_error)?;

    println!("[VALUES]");
    for v in values.iter() {
        println!("  {}:", v.value_type);
        println!("    {}", v.value);
    }

    let secrets = idpw_memo.secrets().map_err(to_io_error)?;

    println!("[SECRETS]");
    for v in secrets.iter() {
        println!("  {}:", v.value_type);
        println!("    {}", v.value);
    }

    let service = idpw_memo.service().map_err(to_io_error)?;

    println!("[LAST UPDATE]");
    if service.time == 0 {
        println!("  ????-??-?? ??:??:??");
    } else {
        let mut time = service.time / 1000;
        let sec = time % 60;
        time /= 60;
        let min = time % 60;
        time /= 60;
        time += TIME_ZONE;
        let hour = time % 24;
        time /= 24;
        let mut year = 1970;
        let mut month = 1;
        let mut day = 1;
        loop {
            let yd = if year % 4 != 0 {
                365
            } else if year % 400 == 0 {
                366 // 1970年から2021年の間に該当は2000年のみ()
            } else if year % 100 == 0 {
                365 // 1970年から2021年の間に該当する年は無い()
            } else {
                366 // 2100年以降は無縁ゆえyear%4の判定だけ実は十分()
            };
            if time < yd {
                let days = [31, 28 + (yd - 365), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                for (i, &d) in days.iter().enumerate() {
                    if time < d {
                        month = i + 1;
                        day = time + 1;
                        break;
                    }
                    time -= d;
                }
                break;
            }
            time -= yd;
            year += 1;
        }
        println!(
            "  {:04}-{:02}-{:02} {:02}:{:02}:{:02}",
            year, month, day, hour, min, sec
        );
    }

    Ok(())
}
